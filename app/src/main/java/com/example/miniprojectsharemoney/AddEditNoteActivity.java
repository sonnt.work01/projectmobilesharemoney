package com.example.miniprojectsharemoney;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.miniprojectsharemoney.db.DAO;
import com.example.miniprojectsharemoney.db.DBHelper;
import com.example.miniprojectsharemoney.model.Note;
import com.example.miniprojectsharemoney.model.NoteDAO;

import java.util.List;

public class AddEditNoteActivity extends AppCompatActivity {

    private static final int MODE_CREATE = 1;
    private static final int MODE_EDIT = 2;

    private EditText edtNoteTitle, edtNoteContent, edtNoteCreateAt;
    private Button btnSave, btnCancel;

    private long notes;
    private boolean needRefresh;
    private int mode;
    private Note note;

    // DBHelper và TodoDAO
    private DBHelper dbHelper;
    private DAO<Note> noteDAO;

    private void initUI(){
        edtNoteTitle = findViewById(R.id.edtNoteTitle);
        edtNoteContent = findViewById(R.id.edtNoteContent);
        edtNoteCreateAt = findViewById(R.id.edtNoteCreateAt);
        btnSave = findViewById(R.id.btnSave);
        btnCancel = findViewById(R.id.btnCancel);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_note);
        initUI();

        btnSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)  {
                buttonSaveClicked();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)  {
                buttonCancelClicked();
            }
        });

        Intent intent = this.getIntent();
        Note note = (Note) intent.getSerializableExtra("note");
        if(note== null)  {
            mode = MODE_CREATE;
        } else  {
            mode = MODE_EDIT;
            edtNoteTitle.setText(note.getTitle());
            edtNoteContent.setText(note.getContent());
            edtNoteCreateAt.setText(note.getCreate_at());
        }
    }

    // User Click on the Save button.
    public void buttonSaveClicked(){
        dbHelper = new DBHelper(this);
        noteDAO = new NoteDAO(dbHelper);

        String title = edtNoteTitle.getText().toString();
        String content = edtNoteContent.getText().toString();
        String createAt = edtNoteCreateAt.getText().toString();

        if(title.equals("") || content.equals("")) {
            Toast.makeText(getApplicationContext(),
                    "Please enter title & content", Toast.LENGTH_LONG).show();
            return;
        }

        if(mode == MODE_CREATE) {
            note = new Note(title, content, createAt);
            notes = noteDAO.create(note);
        } else {
            note.setTitle(title);
            note.setContent(content);
            note.setCreate_at(createAt);
            notes = noteDAO.update(note);
        }

        this.needRefresh = true;

        // Back to MainActivity.
        this.onBackPressed();
    }

    // User Click on the Cancel button.
    public void buttonCancelClicked()  {
        // Do nothing, back MainActivity.
        this.onBackPressed();
    }

    // When completed this Activity,
    // Send feedback to the Activity called it.
    @Override
    public void finish() {

        // Create Intent
        Intent data = new Intent();

        // Request MainActivity refresh its ListView (or not).
        data.putExtra("needRefresh", needRefresh);

        // Set Result
        this.setResult(Activity.RESULT_OK, data);
        super.finish();
    }

}
