package com.example.miniprojectsharemoney;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.miniprojectsharemoney.adapter.EventAdapter;
import com.example.miniprojectsharemoney.adapter.ProductAdapter;
import com.example.miniprojectsharemoney.db.DAO;
import com.example.miniprojectsharemoney.model.Event;
import com.example.miniprojectsharemoney.model.Product;

import java.util.List;

public abstract class ProductDialog extends Dialog {

    private Button btnSave, btnExit;
    private EditText edtName, edtQuantity, edtPrice, edtBuyer, edtEventID;
    private Context context;

    private List<Product> products;
    private DAO<Product> productDAO;

    private ProductAdapter productAdapter;

    public ProductDialog(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public void show() {
        super.show();
        edtName.setText("");
        edtQuantity.setText("");
        edtPrice.setText("");
        edtBuyer.setText("");
        edtEventID.setText("");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_dialog);

        edtName = findViewById(R.id.edtName);
        edtQuantity = findViewById(R.id.edtQuantity);
        edtPrice = findViewById(R.id.edtPrice);
        edtBuyer = findViewById(R.id.edtBuyer);
        edtEventID = findViewById(R.id.edtEventID);
        btnSave = findViewById(R.id.btnSave);
        btnExit = findViewById(R.id.btnExit);

        // Sự kiện khi click vào button
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Xử lý sau
                String name = edtName.getText().toString();
                if(name.isEmpty()) {
                    edtName.setError("Hãy nhập dữ liệu");
                    return;
                }

                int quantity = Integer.parseInt(edtQuantity.getText().toString());
//                if(quantity.isEmpty()){
//                    edtQuantity.setError("Hãy nhập dữ liệu");
//                    return;
//                }

                float price = Integer.parseInt(edtPrice.getText().toString());
//                if(price.isEmpty()){
//                    edtPrice.setError("Hãy nhập dữ liệu");
//                    return;
//                }

                String buyer = edtBuyer.getText().toString();
                if(buyer.isEmpty()){
                    edtBuyer.setError("Hãy nhập dữ liệu");
                    return;
                }

                int event_id = Integer.parseInt(edtEventID.getText().toString());
//                if(event_id.isEmpty()){
//                    edtEventID.setError("Hãy nhập dữ liệu");
//                    return;
//                }

                passData(name, quantity, price, buyer, event_id);
                dismiss();
            }

        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                Toast.makeText(context, "Thoát",Toast.LENGTH_SHORT).show();
            }
        });
    }

    protected abstract void passData(String name, int quantity, float price, String buyer, int event_id);
}