package com.example.miniprojectsharemoney.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DBHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "shareMoney.db";
    private static final int DB_VERSION = 1;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    // Table Names
    private static final String TABLE_EVENT = "events";
    private static final String TABLE_PRODUCT = "products";
    private static final String TABLE_NOTE = "notes";

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Tạo ra bảng trong CSDL (CSDL quan hệ)
        String sql_event = "CREATE TABLE events (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, description TEXT NOT NULL, create_at DATETIME NOT NULL)";
        String sql_product = "CREATE TABLE products (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, quantity INT NOT NULL, price FLOAT NOT NULL, buyer TEXT NOT NULL, event_id INT NOT NULL, FOREIGN KEY ('event_id') REFERENCES events ('id'))";
        String sql_note = "CREATE TABLE notes (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT NOT NULL, content TEXT NOT NULL, create_at DATETIME NOT NULL)";
        db.execSQL(sql_event);
        db.execSQL(sql_product);
        db.execSQL(sql_note);
//        db.execSQL(CREATE_TABLE_PRODUCT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Sẽ chạy khi update (thay đổi cấu trúc bảng) -> tự tìm hiểu thêm
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENT);
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCT);
    }
}
