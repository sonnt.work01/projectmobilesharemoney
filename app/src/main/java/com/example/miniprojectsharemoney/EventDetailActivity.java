package com.example.miniprojectsharemoney;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.miniprojectsharemoney.adapter.ProductAdapter;
import com.example.miniprojectsharemoney.db.DAO;
import com.example.miniprojectsharemoney.db.DBHelper;
import com.example.miniprojectsharemoney.model.Event;
import com.example.miniprojectsharemoney.model.Product;
import com.example.miniprojectsharemoney.model.ProductDAO;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Collections;
import java.util.List;

public class EventDetailActivity extends AppCompatActivity {

    private FloatingActionButton fabAdd;
    private ListView lvProduct;
    private ProductAdapter productAdapter;
    private List<Product> products;
    private ProductDialog productDialog;
    private TextView tvEventId;

    private AlertDialog alertDialog;
    private AlertDialog.Builder builder;

    private Button btnBack;


    // DBHelper và TodoDAO
    private DBHelper dbHelper;
    private DAO<Product> productDAO;

    private void initUI(){
        fabAdd = findViewById(R.id.fabAdd);
        lvProduct = findViewById(R.id.lvProduct);
        btnBack = findViewById(R.id.btnBack);
        tvEventId = findViewById(R.id.tvEventId);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        initUI();

        // init db
        dbHelper = new DBHelper(this);
        productDAO = new ProductDAO(dbHelper);


        Intent intent = getIntent();
        Event event = (Event) intent.getSerializableExtra("event_id");
        long id = Long.parseLong(String.valueOf(event.getId()));

        products = productDAO.getMultipleTable(id);
//        products = Collections.singletonList(productDAO.get(id));
        if(products.size() == 0) Toast.makeText(this, "Danh sách rỗng", Toast.LENGTH_SHORT).show();

        productAdapter = new ProductAdapter(this,products);
        lvProduct.setAdapter(productAdapter);
        // Xử lý sự kiện khi thêm vào listview

        tvEventId.setText(String.valueOf(event.getId()));

        productDialog = new ProductDialog(this) {
            @Override
            protected void passData(String name, int quantity, float price, String buyer, int event_id) {
                Product item = new Product(name, quantity, price, event_id, buyer);
                long id = productDAO.create(item);
                item.setId(id);
                // Push dữ liệu vào product
                products.add(item);
                // Thông báo cho Adapter -> render lại view
                productAdapter.notifyDataSetChanged();
            }
        };

        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Hiển thị lên dialog -> xử lý
                productDialog.show();
                Toast.makeText(EventDetailActivity.this,"Show dialog",Toast.LENGTH_SHORT).show();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        lvProduct.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                //
                builder = new AlertDialog.Builder(EventDetailActivity.this).
                        setTitle("Delete?").
                        setMessage("Bạn có thực sự muốn xóa sản phẩm này???").
                        setPositiveButton("Đúng", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Product item = products.get(position);
                                if(productDAO.delete(item.getId()) == 1){
                                    // XOas thanh cong -> xoa du lieu trong bo nho (RAM)
                                    products.remove(position);
                                    productAdapter.notifyDataSetChanged();
                                }
                            }

                        }).setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(EventDetailActivity.this, "Cancel deletion", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog = builder.create();
                alertDialog.show();
                Toast.makeText(getApplicationContext(), "Clickeddd", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
    }

    @Override
    protected void onDestroy() {
        dbHelper.close();
        super.onDestroy();
    }
}