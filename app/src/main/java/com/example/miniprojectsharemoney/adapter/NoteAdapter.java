package com.example.miniprojectsharemoney.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.miniprojectsharemoney.R;
import com.example.miniprojectsharemoney.model.Event;
import com.example.miniprojectsharemoney.model.Note;

import java.util.List;

public class NoteAdapter extends BaseAdapter {

    private Context context;
    private List<Note> notes;
    private Object MainActivity;

    public NoteAdapter(Context context, List<Note> notes) {
        this.context = context;
        this.notes = notes;
    }

    @Override
    public int getCount() {
        return notes.size();
    }

    @Override
    public Object getItem(int position) {
        return notes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.item_note,parent,false);
        }
        TextView tvTitle, tvDescription, tvCreateAt;
        ImageView imgPopup;
        PopupMenu popupMenu;

        tvTitle = convertView.findViewById(R.id.tvTitle);
        tvCreateAt = convertView.findViewById(R.id.tvCreateAt);
        imgPopup = convertView.findViewById(R.id.imgPopup);

        //set data
        tvTitle.setText(notes.get(position).getTitle());
        tvCreateAt.setText(notes.get(position).getCreate_at());

//         Tạo popup menu
        popupMenu = new PopupMenu(context.getApplicationContext(), imgPopup);
        popupMenu.getMenuInflater().inflate(R.menu.popup_menu,popupMenu.getMenu());

//         Sự kiện khi click vào button
        imgPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Hiển thị popup lên
                popupMenu.show();
            }
        });

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.popup_menu_details:
                        Toast.makeText(context.getApplicationContext(),"Details",Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.popup_menu_edit:
                        Toast.makeText(context.getApplicationContext(),"Edit",Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.popup_menu_delete:
                        notes.remove(position);
                        notifyDataSetChanged();
                        Toast.makeText(context.getApplicationContext(),"Delete",Toast.LENGTH_SHORT).show();
                        break;
                }
                return false;
            }
        });

        return convertView;
    }
}
