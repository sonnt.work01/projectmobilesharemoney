package com.example.miniprojectsharemoney.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.miniprojectsharemoney.R;
import com.example.miniprojectsharemoney.model.Event;
import com.example.miniprojectsharemoney.model.Product;

import java.util.List;

public class ProductAdapter extends BaseAdapter {

    private Context context;
    private List<Product> products;
    private Object MainActivity;

    public ProductAdapter(Context context, List<Product> products) {
        this.context = context;
        this.products = products;
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.product_item,parent,false);
        }
        TextView tvName, tvQuantity, tvPrice, tvBuyer, tvEventId;
        ImageView imgPopup;
        PopupMenu popupMenu;

        tvName = convertView.findViewById(R.id.tvName);
        tvQuantity = convertView.findViewById(R.id.tvQuantity);
        tvPrice = convertView.findViewById(R.id.tvPrice);
        tvBuyer = convertView.findViewById(R.id.tvBuyer);
        tvEventId = convertView.findViewById(R.id.tvEventId);
        imgPopup = convertView.findViewById(R.id.imgPopup);

        //set data
        tvName.setText(products.get(position).getName());
        tvQuantity.setText(String.valueOf(products.get(position).getQuantity()));
        tvPrice.setText(String.valueOf(products.get(position).getPrice()));
        tvBuyer.setText(products.get(position).getBuyer());
        tvEventId.setText(String.valueOf(products.get(position).getEvent_id()));

        // Tạo popup menu
        popupMenu = new PopupMenu(context.getApplicationContext(), imgPopup);
        popupMenu.getMenuInflater().inflate(R.menu.popup_menu,popupMenu.getMenu());

//        // Sự kiện khi click vào button
//        imgPopup.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // Hiển thị popup lên
//                popupMenu.show();
//            }
//        });

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.popup_menu_details:
                        Toast.makeText(context.getApplicationContext(),"Details",Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.popup_menu_edit:
                        Toast.makeText(context.getApplicationContext(),"Edit",Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.popup_menu_delete:
                        products.remove(position);
                        notifyDataSetChanged();
                        Toast.makeText(context.getApplicationContext(),"Delete",Toast.LENGTH_SHORT).show();
                        break;
                }
                return false;
            }
        });

        return convertView;
    }
}
