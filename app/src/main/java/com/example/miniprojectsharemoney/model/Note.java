package com.example.miniprojectsharemoney.model;

import java.io.Serializable;

public class Note implements Serializable {

    private long id;
    private String title;
    private String content;
    private String create_at;

    public Note() {
    }

    public Note(String title, String content, String create_at) {
        this.title = title;
        this.content = content;
        this.create_at = create_at;
    }

    public Note(long id, String title, String content, String create_at) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.create_at = create_at;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }
}
