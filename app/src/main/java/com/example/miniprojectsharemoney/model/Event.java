package com.example.miniprojectsharemoney.model;

import java.io.Serializable;

public class Event implements Serializable {
    private long id;
    private String name;
    private String description;
    private String create_at;

    public Event() {
    }

    public Event(long id, String name, String description, String create_at) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.create_at = create_at;
    }

    public Event(String name, String description, String create_at) {
        this.name = name;
        this.description = description;
        this.create_at = create_at;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }
}
