package com.example.miniprojectsharemoney.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.miniprojectsharemoney.db.DAO;
import com.example.miniprojectsharemoney.db.DBHelper;

import java.util.ArrayList;
import java.util.List;

public class ProductDAO implements DAO<Product> {

    private DBHelper dbHelper;

    public ProductDAO(DBHelper dbHelper){
        this.dbHelper = dbHelper;
    }

    @Override
    public List<Product> all() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "SELECT * FROM products";
        Cursor cursor = db.rawQuery(sql, null);
        // Đưa con trỏ về đầu hàng
        List<Product> list = new ArrayList<>();
        if(cursor.moveToFirst()){
            int idIndex = cursor.getColumnIndex("id");
            int nameIndex = cursor.getColumnIndex("name");
            int quantityIndex = cursor.getColumnIndex("quantity");
            float priceIndex = cursor.getColumnIndex("price");
            int buyerIndex = cursor.getColumnIndex("buyer");
            int event_idIndex = cursor.getColumnIndex("event_id");
            do{
                Product item = new Product();
                // Lấy dữ liệu từ Sqlite -> set giá trị item
                item.setId(cursor.getLong(idIndex));
                item.setName(cursor.getString(nameIndex));
                item.setQuantity(cursor.getInt(quantityIndex));
                item.setPrice(cursor.getFloat((int) priceIndex));
                item.setBuyer(cursor.getString(buyerIndex));
                item.setEvent_id(cursor.getInt(event_idIndex));

                list.add(item);
            }
            while(cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    @Override
    public List<Product> getMultipleTable(long id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "SELECT * FROM products WHERE event_id = "+id;
        Cursor cursor = db.rawQuery(sql, null);
        // Đưa con trỏ về đầu hàng
        List<Product> list = new ArrayList<>();
        if(cursor.moveToFirst()){
            int idIndex = cursor.getColumnIndex("id");
            int nameIndex = cursor.getColumnIndex("name");
            int quantityIndex = cursor.getColumnIndex("quantity");
            float priceIndex = cursor.getColumnIndex("price");
            int buyerIndex = cursor.getColumnIndex("buyer");
            int event_idIndex = cursor.getColumnIndex("event_id");
            do{
                Product item = new Product();
                // Lấy dữ liệu từ Sqlite -> set giá trị item
                item.setId(cursor.getLong(idIndex));
                item.setName(cursor.getString(nameIndex));
                item.setQuantity(cursor.getInt(quantityIndex));
                item.setPrice(cursor.getFloat((int) priceIndex));
                item.setBuyer(cursor.getString(buyerIndex));
                item.setEvent_id(cursor.getInt(event_idIndex));

                list.add(item);
            }
            while(cursor.moveToNext());
        }
        cursor.close();

        return list;

    }

    @Override
    public Product getById(long id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "SELECT * FROM products WHERE event_id = "+id;
        Cursor cursor = db.rawQuery(sql, null);
        Product item = null;
        if(cursor.moveToFirst()){
            int idIndex = cursor.getColumnIndex("id");
            int nameIndex = cursor.getColumnIndex("name");
            int quantityIndex = cursor.getColumnIndex("quantity");
            float priceIndex = cursor.getColumnIndex("price");
            int buyerIndex = cursor.getColumnIndex("buyer");
            int event_idIndex = cursor.getColumnIndex("event_id");
            item  = new Product();
            item.setId(cursor.getLong(idIndex));
            item.setName(cursor.getString(nameIndex));
            item.setQuantity(cursor.getInt(quantityIndex));
            item.setPrice(cursor.getFloat((int) priceIndex));
            item.setBuyer(cursor.getString(buyerIndex));
            item.setEvent_id(cursor.getInt(event_idIndex));
        }
        return item;
    }

    @Override
    public long create(Product item) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("name",item.getName());
        contentValues.put("quantity",item.getQuantity());
        contentValues.put("price",item.getPrice());
        contentValues.put("event_id",item.getEvent_id());
        contentValues.put("buyer",item.getBuyer());
        // Lấy id bản ghi mới
        long id = db.insert("products", null, contentValues);

        return id;
    }

    @Override
    public int update(Product item) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name",item.getName());
        contentValues.put("quantity",item.getQuantity());
        contentValues.put("price",item.getPrice());
        contentValues.put("buyer",item.getBuyer());
        contentValues.put("event_id",item.getEvent_id());
        // VD: update cho bản ghi 1 => UPDATE todos SET title = "Hello" WHERE id = 1
        int rs = db.update("products",contentValues,"id = " + item.getId(), null);
        return rs;
    }

    @Override
    public int delete(long id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int rs = db.delete("products", "id = "+id, null);
        return rs;
    }
}
