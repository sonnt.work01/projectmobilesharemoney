package com.example.miniprojectsharemoney.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.miniprojectsharemoney.db.DAO;
import com.example.miniprojectsharemoney.db.DBHelper;

import java.util.ArrayList;
import java.util.List;

public class EventDAO implements DAO<Event> {

    private DBHelper dbHelper;

    public EventDAO(DBHelper dbHelper){
        this.dbHelper = dbHelper;
    }

    @Override
    public List<Event> all() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "SELECT * FROM events";
        Cursor cursor = db.rawQuery(sql, null);
        // Đưa con trỏ về đầu hàng
        List<Event> list = new ArrayList<>();
        if(cursor.moveToFirst()){
            int idIndex = cursor.getColumnIndex("id");
            int nameIndex = cursor.getColumnIndex("name");
            int descriptionIndex = cursor.getColumnIndex("description");
            int create_atIndex = cursor.getColumnIndex("create_at");
            do{
                Event item = new Event();
                // Lấy dữ liệu từ Sqlite -> set giá trị item
                item.setId(cursor.getLong(idIndex));
                item.setName(cursor.getString(nameIndex));
                item.setDescription(cursor.getString(descriptionIndex));
                item.setCreate_at(cursor.getString(create_atIndex));

                // Push item này vào arraylist (todo)
                list.add(item);
            }
            while(cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    @Override
    public List<Event> getMultipleTable(long id) {
        return null;
    }

    @Override
    public Event getById(long id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "SELECT * FROM events WHERE id = "+id;
        Cursor cursor = db.rawQuery(sql, null);
        Event item = null;
        if(cursor.moveToFirst()){
            int idIndex = cursor.getColumnIndex("id");
            int nameIndex = cursor.getColumnIndex("name");
            int descriptionIndex = cursor.getColumnIndex("description");
            int create_atIndex = cursor.getColumnIndex("create_at");
            item  = new Event();
            item.setId(cursor.getLong(idIndex));
            item.setName(cursor.getString(nameIndex));
            item.setDescription(cursor.getString(descriptionIndex));
            item.setCreate_at(cursor.getString(create_atIndex));
        }
        return item;
    }

    @Override
    public long create(Event item) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("name",item.getName());
        contentValues.put("description",item.getDescription());
        contentValues.put("create_at",item.getCreate_at());
        // Lấy id bản ghi mới
        long id = db.insert("events", null, contentValues );

        return id;
    }

    @Override
    public int update(Event item) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name",item.getName());
        contentValues.put("description",item.getDescription());
        contentValues.put("create_at",item.getCreate_at());
        // VD: update cho bản ghi 1 => UPDATE todos SET title = "Hello" WHERE id = 1
        int rs = db.update("events",contentValues,"id = " + item.getId(), null);
        return rs;
    }

    @Override
    public int delete(long id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int rs = db.delete("events", "id = "+id, null);
        return rs;
    }
}
