package com.example.miniprojectsharemoney.model;

import java.io.Serializable;

public class Product implements Serializable {
    private long id;
    private String name;
    private int quantity;
    private float price;
    private int event_id;
    private String buyer;

    public Product() {
    }

    public Product(long id, String name, int quantity, float price, int event_id, String buyer) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.event_id = event_id;
        this.buyer = buyer;
    }

    public Product(String name, int quantity, float price, String buyer) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.buyer = buyer;
    }

    public Product(String name, int quantity, float price, int event_id, String buyer) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.event_id = event_id;
        this.buyer = buyer;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }
}
