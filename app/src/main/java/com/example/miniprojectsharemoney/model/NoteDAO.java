package com.example.miniprojectsharemoney.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.miniprojectsharemoney.db.DAO;
import com.example.miniprojectsharemoney.db.DBHelper;

import java.util.ArrayList;
import java.util.List;

public class NoteDAO implements DAO<Note> {

    private DBHelper dbHelper;

    public NoteDAO(DBHelper dbHelper){
        this.dbHelper = dbHelper;
    }

    @Override
    public List<Note> all() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "SELECT * FROM notes";
        Cursor cursor = db.rawQuery(sql, null);
        // Đưa con trỏ về đầu hàng
        List<Note> list = new ArrayList<>();
        if(cursor.moveToFirst()){
            int idIndex = cursor.getColumnIndex("id");
            int titleIndex = cursor.getColumnIndex("title");
            int contentIndex = cursor.getColumnIndex("content");
            int createAtIndex = cursor.getColumnIndex("create_at");
            do{
                Note item = new Note();
                // Lấy dữ liệu từ Sqlite -> set giá trị item
                item.setId(cursor.getLong(idIndex));
                item.setTitle(cursor.getString(titleIndex));
                item.setContent(cursor.getString(contentIndex));
                item.setCreate_at(cursor.getString(createAtIndex));

                list.add(item);
            }
            while(cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    @Override
    public List<Note> getMultipleTable(long id) {
        return null;
    }

    @Override
    public Note getById(long id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "SELECT * FROM notes WHERE id = "+id;
        Cursor cursor = db.rawQuery(sql, null);
        Note item = null;
        if(cursor.moveToFirst()){
            int idIndex = cursor.getColumnIndex("id");
            int titleIndex = cursor.getColumnIndex("title");
            int contentIndex = cursor.getColumnIndex("content");
            int createAtIndex = cursor.getColumnIndex("create_at");
            item  = new Note();
            item.setId(cursor.getLong(idIndex));
            item.setTitle(cursor.getString(titleIndex));
            item.setContent(cursor.getString(contentIndex));
            item.setCreate_at(cursor.getString(createAtIndex));
        }
        return item;
    }

    @Override
    public long create(Note item) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("title",item.getTitle());
        contentValues.put("content",item.getContent());
        contentValues.put("create_at",item.getCreate_at());
        // Lấy id bản ghi mới
        long id = db.insert("notes", null, contentValues);

        return id;
    }

    @Override
    public int update(Note item) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("title",item.getTitle());
        contentValues.put("content",item.getContent());
        contentValues.put("create_at",item.getCreate_at());
        // VD: update cho bản ghi 1 => UPDATE todos SET title = "Hello" WHERE id = 1
        int rs = db.update("notes",contentValues,"id = " + item.getId(), null);
        return rs;
    }

    @Override
    public int delete(long id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int rs = db.delete("notes", "id = "+id, null);
        return rs;
    }
}
