package com.example.miniprojectsharemoney;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.miniprojectsharemoney.adapter.EventAdapter;
import com.example.miniprojectsharemoney.adapter.NoteAdapter;
import com.example.miniprojectsharemoney.db.DAO;
import com.example.miniprojectsharemoney.db.DBHelper;
import com.example.miniprojectsharemoney.model.Event;
import com.example.miniprojectsharemoney.model.EventDAO;
import com.example.miniprojectsharemoney.model.Note;
import com.example.miniprojectsharemoney.model.NoteDAO;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class NoteActivity extends AppCompatActivity {

    private FloatingActionButton fabAdd;
    private Button btnBack;
    private ListView lvNote;
    private NoteAdapter noteAdapter;
    private List<Note> notes;

    private AlertDialog alertDialog;
    private AlertDialog.Builder builder;

    // DBHelper và TodoDAO
    private DBHelper dbHelper;
    private DAO<Note> noteDAO;

    private void initUI(){
        fabAdd = findViewById(R.id.fabAdd);
        lvNote = findViewById(R.id.lvNote);
        btnBack = findViewById(R.id.btnBack);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        initUI();

        // init db
        dbHelper = new DBHelper(this);
        noteDAO = new NoteDAO(dbHelper);

        notes = noteDAO.all();
        if(notes.size() == 0) Toast.makeText(this, "Danh sách rỗng", Toast.LENGTH_SHORT).show();

        noteAdapter = new NoteAdapter(this,notes);
        lvNote.setAdapter(noteAdapter);
        // Xử lý sự kiện khi thêm vào listview

//        eventDialog = new EventDialog(this) {
//            @Override
//            protected void passData(String name, String description, String create_at) {
//                Event item = new Event(name, description, create_at);
//                long id = eventDAO.create(item);
//                item.setId(id);
//                // Push dữ liệu vào contact
//                events.add(item);
//                // Thông báo cho Adapter -> render lại view
//                eventAdapter.notifyDataSetChanged();
//            }
//        };

        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent intent = new Intent(NoteActivity.this, AddEditNoteActivity.class);
               startActivity(intent);
            }
        });

//        lvEvent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Intent intent = new Intent(MainActivity.this, EventDetailActivity.class);
//                intent.putExtra("event_id", events.get(position));
//                startActivity(intent);
//            }
//        });

        lvNote.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                //
                builder = new AlertDialog.Builder(NoteActivity.this).
                        setTitle("Delete?").
                        setMessage("Bạn có thực sự muốn xóa người này???").
                        setPositiveButton("Đúng", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Note item = notes.get(position);
                                if(noteDAO.delete(item.getId()) == 1){
                                    // XOas thanh cong -> xoa du lieu trong bo nho (RAM)
                                    notes.remove(position);
                                    noteAdapter.notifyDataSetChanged();
                                }
                            }

                        }).setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(NoteActivity.this, "Cancel deletion", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog = builder.create();
                alertDialog.show();
                Toast.makeText(getApplicationContext(), "Clickeddd", Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NoteActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onDestroy() {
        dbHelper.close();
        super.onDestroy();
    }
}