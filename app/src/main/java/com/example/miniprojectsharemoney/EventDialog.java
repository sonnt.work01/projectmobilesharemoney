package com.example.miniprojectsharemoney;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.miniprojectsharemoney.adapter.EventAdapter;
import com.example.miniprojectsharemoney.db.DAO;
import com.example.miniprojectsharemoney.model.Event;

import java.util.List;

public abstract class EventDialog extends Dialog {

    private Button btnSave, btnExit;
    private EditText edtName, edtDescription, edtCreateAt;
    private Context context;

    private List<Event> contacts;
    private DAO<Event> contactDAO;

    private EventAdapter eventAdapter;

    public EventDialog(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public void show() {
        super.show();
        edtName.setText("");
        edtDescription.setText("");
        edtCreateAt.setText("");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_dialog);

        edtName = findViewById(R.id.edtName);
        edtDescription = findViewById(R.id.edtDescription);
        edtCreateAt = findViewById(R.id.edtCreateAt);
        btnSave = findViewById(R.id.btnSave);
        btnExit = findViewById(R.id.btnExit);

        // Sự kiện khi click vào button
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Xử lý sau
                String name = edtName.getText().toString();
                if(name.isEmpty()) {
                    edtName.setError("Hãy nhập dữ liệu");
                    return;
                }

                String description = edtDescription.getText().toString();
                if(description.isEmpty()){
                    edtDescription.setError("Hãy nhập dữ liệu");
                    return;
                }

                String createAt = edtCreateAt.getText().toString();
                if(createAt.isEmpty()){
                    edtCreateAt.setError("Hãy nhập dữ liệu");
                    return;
                }

                passData(name,description,createAt);
                dismiss();
            }

        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                Toast.makeText(context, "Thoát",Toast.LENGTH_SHORT).show();
            }
        });
    }

    protected abstract void passData(String name, String description, String creatAt);
}